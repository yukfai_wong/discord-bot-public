const xmlbuilder = require('xmlbuilder2');
require('dotenv').config();

const tts_options = {
    method: 'POST',
    baseURL: 'https://southeastasia.tts.speech.microsoft.com/',
    url: 'cognitiveservices/v1',
    headers: {
        'Ocp-Apim-Subscription-Key': process.env.SPEECH_SERVICE_KEY2,
        'X-Microsoft-OutputFormat': 'audio-24khz-160kbitrate-mono-mp3',
        'Content-Type': 'application/ssml+xml'
    },
    data: '',
    responseType: 'stream'
};


function textToSpeech(text, lang, voice, voice_style) {
    // Create the SSML request.
    let xml_req = xmlbuilder
    	.create({ version: '1.0' })
        .ele('speak')
        .att('version', '1.0')
        .att('xml:lang', lang)
        .att('xmlns', "http://www.w3.org/2001/10/synthesis")
        .att('xmlns:mstts', "https://www.w3.org/2001/mstts")

        .ele('voice')
        .att('name', voice)

        

    if(!(voice_style === '')){
    	xml_req
    	.ele('mstts:express-as')
        .att('style', voice_style)
        .att('styledegree', "1.5")
    }

    xml_req.txt(text)
    // Convert the XML into a string to send in the TTS request.
    tts_options['data'] = xml_req.end().toString();
    return tts_options;
};


// Use async and await to get the token before attempting
// to convert text to speech.
function tts(text, lang, voice, voice_style, rate) {
    try {
        return textToSpeech(text, lang, voice, voice_style, rate);
    } catch (err) {
        console.log(`Something went wrong: ${err}`);
    }
}

module.exports = {
  tts: tts
};
