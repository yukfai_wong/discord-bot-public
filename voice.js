const Voice = require('@discordjs/voice');

class PlayerControl{
  playlist = []
  player = Voice.createAudioPlayer();
  isplaying =  false;
  constructor(id){
    this.id = id
    this.player.on('idle', e => this.playNext(e))
    this.player.on('error', error => {});
  }
  playNext(e){
    if(this.playlist.length > 0){
      this.isplaying = true;
      let recourse = this.playlist.shift();
      console.log("%d playing: %s", this.id, recourse.text)
      this.player.play(recourse);
    }
    else{
      this.isplaying = false;
    }
  }

  play(recourse){
    if(this.isplaying){
      this.playlist.push(recourse)
    }
    else{
      this.isplaying = true;
      console.log("%d playing: %s", this.id, recourse.text)
      this.player.play(recourse);
    }
  }
  

}

class VoiceControl {
  playstatus = []
  

  async join(message) {

    let connection = Voice.getVoiceConnection(message.guild.id);
    

    if(connection === undefined || connection.joinConfig.channelId != message.member.voice.channel.id){
      this.playstatus[message.guild.id] = new PlayerControl(message.guild.id);

      connection = Voice.joinVoiceChannel({
        channelId: message.member.voice.channel.id,
        guildId: message.guild.id,
        adapterCreator: message.guild.voiceAdapterCreator
      });
      connection.subscribe(this.playstatus[message.guild.id].player);

      return new Promise(resolve => {
                        connection.on("ready", async() => {
                        resolve();
                      });
                    });
    }
    return new Promise(resolve => {
      resolve();
    });
  }

  play(message, buffer, text) {
    let resource = Voice.createAudioResource(buffer)
    resource.text = text
    this.playstatus[message.guild.id].play(resource)
  }
}

module.exports = VoiceControl;
