const axios = require('axios');
const dotenv = require('dotenv');

const Discord = require('discord.js');

const TTS = require('./tts.js');
const VoiceControl = require('./voice.js');

dotenv.config();
// Initialize Discord bot
const client = new Discord.Client({intents: [Discord.Intents.FLAGS.GUILDS, 
                                            Discord.Intents.FLAGS.GUILD_MESSAGES, 
                                            Discord.Intents.FLAGS.GUILD_VOICE_STATES]});

client.on('ready', () => {
  console.log("Ready");
  client.user.setPresence({ activity: { name: '輸入!h查看指令' }, status: 'online' });
});
client.login(process.env.DISCORD_TOKEN);

// Global variable

const voicecontrol = new VoiceControl();

let speech_switch_on_id = new Set();

const reply = ["實在想不出有什麼語言可以和你溝通",
               "別跟我說話，我有潔癖",
               "別以為有愛護動物協會撐腰就可以為所欲為",
               "沒文化也要講人話啊",
               "没有跟垃圾桶聊天的習慣",
               "把你臉上的解析度調低點好嗎？高清真的不適合你",
               "我不是獸醫，別找我",
               "你五官組合很模糊",
               "遇見你之後世界全黑了"
              ]

const streamOptions = { seek: 0, volume: 1 , passes: 3};
const tts = async function (message, text, lang, voice, voice_style, rate) {
  if(text.length == 0){
    return;
  }
  if(text.length>30){
    message.channel.send("太長了我不想讀");
    return;
  }
  if(!message.member.voice.channel){
    message.channel.send("在語音頻道裏才能說話");
    return;
  }
  text = text.replace(/\ /g, ",");

  let post_req = TTS.tts(text, lang, voice, voice_style, rate);
  let req = axios(post_req);

  voicecontrol.join(message).then(()=>{
    req.then(function (response) {
      console.log("%d Downloading: %s", message.guild.id, text);
      voicecontrol.play(message, response.data, text)
    })
    .catch(err =>{
          console.log(err);
    });
  });
};

function isNormalInteger(str) {
    return /^\d+$/.test(str);
}
let blacklist = {}
const authorised = ["341235651804659722", "320170601606021120", "305614711846600705"
                    , "230698772001193984"];
const blist = async function (message, args) {
  let isauthorised = false;
  authorised.forEach(function(id) {
    if(message.author.id === id){
      isauthorised = true;
    }
  });
  if(isauthorised){
    args = args.filter(function (arg){return !(arg==="")});
    let black = args[0].substring(2, args[0].length - 1);
    let time = args[1].split(':');
    if(!(isNormalInteger(time[0]) && isNormalInteger(time[1]))){
      message.channel.send("時間輸入錯鋘");
      return
    }
    let hour = parseInt(time[0]);
    let min = parseInt(time[1]);
    if(!(args[0].substring(0,1)==="<")){
      message.channel.send("要艾特一個人");
      return
    }
    let current = new Date();
    current.setHours(current.getHours()+hour);
    current.setMinutes(current.getMinutes()+min);
    blacklist[black] = current;
    message.channel.send(args[0] + "這個壞孩子已被加入被屏蔽豪華套餐, "
                       +(hour!=0 ? hour+"小時 ": "")
                       +(min!=0 ? min+"分鐘 ": "")
                       +"内我不會理他");
  }
}
const free = async function (message, args) {
  let isauthorised = false;
  args = args.filter(function (arg){return !(arg==="")});
  let black = args[0].substring(2, args[0].length - 1);
  if(!(args[0].substring(0,1)==="<")){
    message.channel.send("要艾特一個人");
    return
  }
  authorised.forEach(function(id) {
    if(message.author.id === id){
      isauthorised = true;
    }
  });
  if(isauthorised){
    delete blacklist[black]
    message.channel.send(args[0] + "這個壞孩子已被放出來");
  }
}

const speech_switch = async function (message) {
  if(speech_switch_on_id.has(message.author.id)){
    speech_switch_on_id.delete(message.author.id);
  }else{
    speech_switch_on_id.add(message.author.id);
  }
}

client.on('messageCreate', async function (message) {
  let s = message.content.substring(0, 1);
  if ( s == '=' || s == '.' || s == '。') {
    let text = message.content.substring(1);

    let match_cmd = new RegExp(/^[^\s]*/g);
    let cmd = match_cmd.exec(text)[0].toLowerCase();
    if((cmd === null) || (cmd === '')){//not found or case like --> . asd
      return
    }
    args = text.substring(match_cmd.lastIndex+1);
    console.log("%d cmd: %s; arg: %s", message.guild.id ,cmd , args);
    switch(cmd) {
      case '1':
      case 'v':
        await tts(message, args, "zh-CN", "zh-CN-XiaoxiaoNeural", "");
        return;
      break;
      case 'sad':
        await tts(message, args, "zh-CN", "zh-CN-XiaoxiaoNeural", "sad");
        return;
      break;
      case 'angry':
        await tts(message, args, "zh-CN", "zh-CN-XiaoxiaoNeural", "angry");
        return;
      break; 
      case '2':
      case 'b':
        await tts(message, args, "zh-CN", "zh-CN-XiaoyouNeural", "");
        return;
      break;
      case 'jp':
        await tts(message, args, "ja-JP", "ja-JP-NanamiNeural", "");
        return;
      break;
      case 'hk':
        await tts(message, args, "zh-HK", "zh-HK-HiuMaanNeural", "");
        return;
      break;
      case 'tw':
        await tts(message, args, "zh-TW", "zh-TW-HsiaoChenNeural", "");
        return;
      break;
      case 'blist':
        blist(message, args);
        return;
      break;
      case 'free':
        free(message, args);
        return;
      break;
      case 'ss':
      case 'speech_switch':
        speech_switch(message);
        return;
      break;
      case 'h':
      	message.channel.send("```.v和.1: 用禦姐音代你說話\n.b和.2: 用蘿莉音代你說話\n.jp 日語\n.hk 粵語\n.tw 比較偏向於台灣讀音的普通話\n.angry 生氣的語氣\n.sad 非常悲傷\n\ne.g.:\n.v 例子\n\n為了方便.可以用。和=代替\n為了更加方便.ss 可以打開一個自動說話開關，你發出的信息會全部自動說出來。再輸入一次.ss就可以關閉```");
      	return;
      break;
      default:
        message.channel.send("啥？");
    }
  }
  if(speech_switch_on_id.has(message.author.id)){
    await tts(message, message.content, "zh-CN", "zh-CN-XiaoxiaoNeural", "Chat", '1.05');
  }
});
