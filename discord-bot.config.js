module.exports = {
    apps : [
        {
          name: "discord-bot-pub",
          script: "./main.js",
          watch: true,
          cron_restart: '0 7 * * *',
        }
    ]
  }